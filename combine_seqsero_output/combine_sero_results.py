
# USAGE
# python3 path/to/combine_sero_results.py /path/to/dir/containing/individual/results

import glob, sys
import pandas as pd


if len(sys.argv) < 2:
    print("Incorrect usage.\nUsage:\npython3 combine_sero_results.py /path/to/dir/containing/individual/results")
    sys.exit()
results_dir = sys.argv[1]

all_results = {}
colnames = []
first = True
# iterate through all .txt files
# You may want to keep them in a separate folder
# cd into that folder before running the code
for filepath in glob.iglob(rf'{results_dir}/SeqSero_result*/SeqSero_result.txt'):
    print(filepath)
    sample_results = []
    with open(filepath, 'r') as fh:
        for line in fh:
            line = line.strip()
            if line.startswith("Output") or line.startswith("Note") or line == "":
                continue
            elif line.startswith("Input"):
                filename = line.split("\t")[1].split("/")[-1]
                if filename.endswith("_1.fastq.gz"):
                    filename = filename[:-len("_1.fastq.gz")]
                print(filename)
                continue
            else:
                results = line.split("\t")
                k,v = results[0], results[1]
                sample_results.append(v)
                if first:
                    colnames.append(k)
    first = False
    all_results[filename] = sample_results

print(all_results)
print(colnames)

output_df = pd.DataFrame.from_dict(all_results, orient='index', columns=colnames)
output_df.index.name = "Samples"
output_df.to_csv(f"{results_dir}/seqsero_summary.csv", encoding='utf-8')


