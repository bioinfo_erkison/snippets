Output_directory:	SeqSero_result_05_07_2021_16_46_002672102
Input files:	/data/home/Ayorinde_Afo/projects/salmonella_project/fastqs/INOS60_1.fastq.gz	/data/home/Ayorinde_Afo/projects/salmonella_project/fastqs/INOS60_2.fastq.gz
O antigen prediction:	8
H1 antigen prediction(fliC):	b
H2 antigen prediction(fljB):	z6
Predicted subspecies:	I
Predicted antigenic profile:	8:b:z6
Predicted serotype:	Tounouma or Banalia
Note: The predicted serotypes share the same general formula:	8:b:z6

