# Combine Seqsero output

### Dependencies
- Pandas

### Usage
`python3 combine_seqsero_output.py <results_dir>`  
**Note**: The results_dir must contain all the individual seqsero output directories 


### Output
This script outputs a .csv file summary of all the seqsero results in the supplied results directory
