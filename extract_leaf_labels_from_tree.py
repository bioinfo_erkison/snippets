#!/data/miniconda3/envs/shared_env/bin/python3
import argparse
import dendropy
import csv

def extract_leaf_labels_from_tree(tree_file):
    tree = dendropy.Tree.get_from_string(open(tree_file).read(), schema='newick')
    tips_labels = [node.taxon.label.replace(" ", "_") for node in tree.leaf_nodes()]
    return tips_labels

def create_output_file(output_file, label):
    with open(output_file,'w',newline='') as csvfile:
        fieldnames = ['id']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)   
        writer.writeheader()
        #num = 0
        for each in label :
            lflbl = str(each)
            #num += 1
            writer.writerow({'id': lflbl})
    #print ('Success! File Saved!')

def parse_arguments():
    description = """
    A script to extract leaf labels from a newick tree.

    """
    # parse all arguments
    parser = argparse.ArgumentParser(description=description,formatter_class=argparse.RawDescriptionHelpFormatter,)
    parser.add_argument('-o', '--output_filepath', help='path to output file', required=True)
    parser.add_argument('-t', '--tree_filepath', help='path to tree file', required=True)

    options = parser.parse_args()
    return options

def main(output_filepath, tree_filepath = None):
    labels = extract_leaf_labels_from_tree(tree_filepath)
    create_output_file(output_filepath, labels)


if __name__ == "__main__":
    options = parse_arguments()
    main(options.output_filepath, options.tree_filepath)
