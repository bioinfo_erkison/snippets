#!/data/miniconda3/envs/shared_env/bin/python3
import csv
import subprocess
import argparse

def parse_arguments():
    description = """
    A script to list the fastq files in the specified file path 

    """
    parser = argparse.ArgumentParser(description=description,formatter_class=argparse.RawDescriptionHelpFormatter,)
    parser.add_argument('-f', '--fastq_filepath', help='path to the fastq files',required=True)
    parser.add_argument('-o', '--output_filename', help='name of the output file',required=True)
    options = parser.parse_args()
    return options

def create_output(fastq_filepath,output_filename):
    file_path = fastq_filepath
    output = "%s.csv" % output_filename
    with open(output,mode='w',newline='') as out_file:
        writer = csv.writer(out_file)
        cmd = subprocess.Popen(['ls',file_path], stdout=subprocess.PIPE)
        fastq_files = cmd.stdout
        fastq_lst = list()
        for fastq_file in fastq_files:
            fastq_file= str(fastq_file, encoding='utf-8')
            fastq_file.strip()
            fastq_lst.append(fastq_file)
        for any in fastq_lst:
            writer.writerow([any])
        print ("Success: Fastq file names have been written to "+"%s.csv" % output_filename)

def main(fastq_filepath, output_filename = None):
    create_output(fastq_filepath,output_filename)

if __name__ == "__main__":
    options = parse_arguments()
    main(options.fastq_filepath, options.output_filename)
        
